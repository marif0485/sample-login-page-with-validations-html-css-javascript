const inputs = document.querySelectorAll(".input");

function addcl() {
  let parent = this.parentNode.parentNode;
  parent.classList.add("focus");
}

function remcl() {
  let parent = this.parentNode.parentNode;
  if (this.value == "") {
    parent.classList.remove("focus");
  }
}

inputs.forEach((input) => {
  input.addEventListener("focus", addcl);
  input.addEventListener("blur", remcl);
});

function login() {
  const username = document.getElementById("username").value;
  const password = document.getElementById("password").value;

  const username_error = document.getElementById("username-error");
  const password_error = document.getElementById("password-error");
  if (username === "admin" && password === "password") {
    username_error.classList.remove("show-error");
    password_error.classList.remove("show-error");
    alert("Login Successful");
  } else {
    if (username !== "admin") {
      username_error.classList.add("show-error");
    }else{
		username_error.classList.remove("show-error");
	}
    if (password !== "password") {
      password_error.classList.add("show-error");
    }else{
		password_error.classList.remove("show-error");
	}
  }
}
